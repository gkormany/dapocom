<?php

namespace App\Application\Actions\Search;

use App\Application\Actions\Action;
use App\Model\EbayProductFeedFetch;
use App\Model\Facade\ProductFeedFetchInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;


class SearchAction extends Action
{
    /**
     * @var EbayProductFeedFetch
     */
    private EbayProductFeedFetch $productFeedFetch;

    /**
     * SearchAction constructor.
     * @param LoggerInterface      $logger
     * @param EbayProductFeedFetch $productFeedFetch
     */
    public function __construct(LoggerInterface $logger, EbayProductFeedFetch $productFeedFetch)
    {
        parent::__construct($logger);
        $this->productFeedFetch = $productFeedFetch;
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $params   = $this->request->getQueryParams();
        $keywords = filter_var($params['keywords'] ?? null, FILTER_SANITIZE_STRING);
        $minPrice = $params['min_price'] ?? null;
        $maxPrice = $params['max_price'] ?? null;
        $sorting  = $params['sorting'] ?? ProductFeedFetchInterface::DEFAULT_SORTING;

        return $this->respondWithData($this->productFeedFetch->executeSearch($keywords, $minPrice, $maxPrice, $sorting)->getValues());
    }
}
