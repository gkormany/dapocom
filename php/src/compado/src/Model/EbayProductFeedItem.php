<?php

namespace App\Model;

use App\Model\Facade\ProductFeedItemInterface;
use App\Model\Traits\ProductFeedItemSerialize;
use JsonSerializable;

class EbayProductFeedItem implements ProductFeedItemInterface, JsonSerializable
{
    use ProductFeedItemSerialize;

    private static string $provider = 'EBay';

    private $itemId;

    private $title;

    private $viewItemURL;

    private $shippingCost;

    private $currency;

    private $price;

    private $timeEnd;

    private $pictureUrl;

    private $description;

    private $brand;

    /**
     * EbayProductFeedItem constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->itemId       = $data['itemId'][0] ?? null;
        $this->title        = $data['title'][0] ?? '';
        $this->description  = $data['subtitle'][0] ?? '';
        $this->shippingCost = $data['shippingInfo'][0]['shippingServiceCost'][0]['__value__'] ?? null;
        $this->currency     = $data['sellingStatus'][0]['currentPrice'][0]['@currencyId'] ?? null;
        $this->price        = $data['sellingStatus'][0]['currentPrice'][0]['__value__'] ?? null;
        $this->timeEnd      = $data['listingInfo'][0]['endTime'][0] ?? null;
        $this->pictureUrl   = $data['galleryURL'][0] ?? null;
        $this->viewItemURL  = $data['viewItemURL'][0] ?? null;
        $this->brand        = $data['aspectHistogramContainer'][0]['aspect'][0]['brand'] ?? null;
    }

    /**
     * @param array $data
     * @return EbayProductFeedItem
     */
    public static function fromArray(array $data): EbayProductFeedItem
    {
        return new EbayProductFeedItem($data);
    }

    public function getProvider(): string
    {
        return static::$provider;
    }

    public function getItemId()
    {
        return $this->itemId;
    }

    public function getMainPhotoUrl(): ?string
    {
        return $this->pictureUrl;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getPriceCurrency(): string
    {
        return $this->currency;
    }

    public function getShippingPrice(): ?float
    {
        return $this->shippingCost;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getValidUntil()
    {
        return $this->timeEnd;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function getClickOutLink(): string
    {
        return $this->viewItemURL;
    }
}
