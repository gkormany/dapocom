<?php

namespace App\Model;

use App\Model\Facade\ProductFeedFetchInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Utils;

/**
 * EbayProductFeedFetch
 */
class EbayProductFeedFetch implements ProductFeedFetchInterface
{
    /**
     * @var ClientInterface
     */
    private ClientInterface $client;

    /**
     * EbayProductFeedFetch constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * executeSearch
     *
     * @param string $keywords
     * @param mixed  $priceMin
     * @param mixed  $priceMax
     * @param string $sorting
     * @return ProductFeedItemCollection
     */
    public function executeSearch(string $keywords, $priceMin = null, $priceMax = null, string $sorting = ProductFeedFetchInterface::DEFAULT_SORTING): ProductFeedItemCollection
    {
        $filterCount = 0;
        $queryObj    = [
            'outputSelector(0)' => 'GalleryInfo',
            'outputSelector(1)' => 'AspectHistogram',
            'outputSelector(2)' => 'PictureURLSuperSize',
            'keywords'          => $keywords
        ];


        if (filter_var($priceMin, FILTER_SANITIZE_NUMBER_FLOAT)) {
            $queryObj["itemFilter({$filterCount}).name"]  = 'MinPrice';
            $queryObj["itemFilter({$filterCount}).value"] = $priceMin;
            $filterCount++;
        }

        if (filter_var($priceMax, FILTER_SANITIZE_NUMBER_FLOAT)) {
            $queryObj["itemFilter({$filterCount}).name"]  = 'MaxPrice';
            $queryObj["itemFilter({$filterCount}).value"] = $priceMax;
            $filterCount++;
        }

        if ($sorting === ProductFeedFetchInterface::PRICE_ASC) {
            $queryObj['sortOrder'] = 'PricePlusShippingLowest';
        }

        try {
            $res = $this->client->get(null, [
                'query' => $queryObj
            ]);
        } catch (TransferException $e) {
            // TODO: do something with the exception
            throw $e;
        }

        if (!$res->getBody()) {
            return new ProductFeedItemCollection();
        }

        $resArr = Utils::jsonDecode($res->getBody()->getContents(), true);

        echo $res->getBody()->getContents();

        $items = $resArr['findItemsByKeywordsResponse']['0']['searchResult'][0]['item'] ?? [];

        return new ProductFeedItemCollection(array_map([EbayProductFeedItem::class, 'fromArray'], $items));
    }
}
