<?php

namespace App\Model;

use App\Model\Facade\ProductFeedItemInterface;
use Doctrine\Common\Collections\ArrayCollection;
use InvalidArgumentException;

/**
 * Class ProductFeedItemCollection
 * @package App\Model
 */
class ProductFeedItemCollection extends ArrayCollection
{
    /**
     * ProductFeedItemCollection constructor.
     * @param array $elements
     */
    public function __construct(array $elements = [])
    {
        $elements = array_filter($elements, function ($element) {
            return $element instanceof ProductFeedItemInterface;
        });

        parent::__construct($elements);
    }

    /**
     * @param mixed $element
     * @return $this
     * @throws InvalidArgumentException
     */
    public function add($element): self
    {
        if (!($element instanceof ProductFeedItemInterface)) {
            throw new InvalidArgumentException('Element must be type of ' . ProductFeedItemInterface::class . ' but ' . get_class($element) . ' was given');
        }

        parent::add($element);

        return $this;
    }

    /**
     * @param mixed $key
     * @param mixed $value
     * @return $this
     * @throws InvalidArgumentException
     */
    public function set($key, $value): self
    {
        if (!($value instanceof ProductFeedItemInterface)) {
            throw new InvalidArgumentException('Element must be type of ' . ProductFeedItemInterface::class . ' but ' . get_class($value) . ' was given');
        }

        parent::set($key, $value);

        return $this;
    }
}
