<?php

namespace App\Model\Traits;

use App\Model\Facade\ProductFeedItemInterface;

trait ProductFeedItemSerialize
{
    public function jsonSerialize(): array
    {
        if (!$this instanceof ProductFeedItemInterface) {
            return [];
        }

        return [
            'provider'        => $this->getProvider(),
            'item_id'         => $this->getItemId(),
            '$click_out_link' => $this->getClickOutLink(),
            'main_photo_url'  => $this->getMainPhotoUrl(),
            'price'           => $this->getPrice(),
            'price_currency'  => $this->getPriceCurrency(),
            'shipping_price'  => $this->getShippingPrice(),
            'title'           => $this->getTitle(),
            'description'     => $this->getDescription(),
            'valid_until'     => $this->getValidUntil(),
            'brand'           => $this->getBrand()
        ];
    }
}
