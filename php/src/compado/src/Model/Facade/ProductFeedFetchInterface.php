<?php

namespace App\Model\Facade;

use App\Model\ProductFeedItemCollection;

interface ProductFeedFetchInterface
{
    public const DEFAULT_SORTING = 'default';
    public const PRICE_ASC       = 'price_asc';

    public function executeSearch(string $keywords, $priceMin = null, $priceMax = null, string $sorting = self::DEFAULT_SORTING): ProductFeedItemCollection;
}
