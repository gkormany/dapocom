<?php 

namespace App\Model\Facade;

interface ProductFeedItemInterface
{
    public function getProvider(): string;

    public function getItemId();

    public function getClickOutLink(): string;

    public function getMainPhotoUrl(): ?string;

    public function getPrice(): float;

    public function getPriceCurrency(): string;

    public function getShippingPrice(): ?float;

    public function getTitle(): string;

    public function getDescription(): ?string;

    public function getValidUntil();

    public function getBrand(): ?string;
}
