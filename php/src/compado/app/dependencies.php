<?php
declare(strict_types=1);

use App\Application\Settings\SettingsInterface;
use App\Model\EbayProductFeedFetch;
use DI\ContainerBuilder;
use GuzzleHttp\Client;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsInterface::class);

            $loggerSettings = $settings->get('logger');
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
    ]);

    $containerBuilder->addDefinitions([
        EbayProductFeedFetch::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsInterface::class);
            $ebayGuzzleClientSettings = $settings->get('ebay_product_feed');

            $ebayGuzzleClient = new Client($ebayGuzzleClientSettings);

            return new EbayProductFeedFetch($ebayGuzzleClient);
        }
    ]);
};
