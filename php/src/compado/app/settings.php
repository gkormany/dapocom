<?php
declare(strict_types=1);

use App\Application\Settings\Settings;
use App\Application\Settings\SettingsInterface;
use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {

    // Global Settings Object
    $containerBuilder->addDefinitions([
        SettingsInterface::class => function () {
            return new Settings([
                'displayErrorDetails' => true, // Should be set to false in production
                'logger'              => [
                    'name'  => 'slim-app',
                    'path'  => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                    'level' => Logger::DEBUG,
                ],
                'ebay_product_feed'   => [
                    'base_url' => 'https://svcs.sandbox.ebay.com/services/search/FindingService/v1',
                    'defaults'  => [
                        'headers' => [
                            'X-EBAY-SOA-GLOBAL-ID'            => 'EBAY-US',
                            'X-EBAY-SOA-OPERATION-NAME'       => 'findItemsByKeywords',
                            'X-EBAY-SOA-SERVICE-VERSION'      => '1.0.0',
                            'X-EBAY-SOA-SECURITY-APPNAME'     => 'WandoInt-217b-42d8-a699-e79808dd505e',
                            'X-EBAY-SOA-RESPONSE-DATA-FORMAT' => 'JSON'
                        ]
                    ],
                ]
            ]);
        }
    ]);
};
